<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9036975f-a565-4728-84b8-2838cf32029a(Secret.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="5xc7" ref="r:cbde3a60-d7cb-4f79-b8b8-0cf8998202e6(Secret.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1153422305557" name="jetbrains.mps.baseLanguage.structure.LessThanOrEqualsExpression" flags="nn" index="2dkUwp" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="1147467115080" name="jetbrains.mps.lang.constraints.structure.NodePropertyConstraint" flags="ng" index="EnEH3">
        <reference id="1147467295099" name="applicableProperty" index="EomxK" />
        <child id="1212097481299" name="propertyValidator" index="QCWH9" />
        <child id="1152963095733" name="propertySetter" index="1LXaQT" />
      </concept>
      <concept id="1147468365020" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_node" flags="nn" index="EsrRn" />
      <concept id="1212096972063" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_PropertyValidator" flags="in" index="QB0g5" />
      <concept id="1152959968041" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_PropertySetter" flags="in" index="1LLf8_" />
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213098023997" name="property" index="1MhHOB" />
      </concept>
      <concept id="1153138554286" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_propertyValue" flags="nn" index="1Wqviy" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="6bosOKdCF3y">
    <ref role="1M2myG" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
    <node concept="EnEH3" id="6bosOKdCF3z" role="1MhHOB">
      <ref role="EomxK" to="5xc7:3JjgIMhdUzN" resolve="breite" />
      <node concept="QB0g5" id="6bosOKdCF3_" role="QCWH9">
        <node concept="3clFbS" id="6bosOKdCF3A" role="2VODD2">
          <node concept="3clFbF" id="6bosOKdCFaV" role="3cqZAp">
            <node concept="1Wc70l" id="6bosOKdCI5q" role="3clFbG">
              <node concept="2dkUwp" id="6bosOKdCJka" role="3uHU7w">
                <node concept="3cmrfG" id="6bosOKdCJ_n" role="3uHU7w">
                  <property role="3cmrfH" value="100" />
                </node>
                <node concept="1Wqviy" id="6bosOKdCIj6" role="3uHU7B" />
              </node>
              <node concept="3eOSWO" id="6bosOKdCGn8" role="3uHU7B">
                <node concept="1Wqviy" id="6bosOKdCFaU" role="3uHU7B" />
                <node concept="3cmrfG" id="6bosOKdCGuH" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1LLf8_" id="6bosOKdEeee" role="1LXaQT">
        <node concept="3clFbS" id="6bosOKdEeef" role="2VODD2">
          <node concept="3clFbF" id="6bosOKdEese" role="3cqZAp">
            <node concept="37vLTI" id="6bosOKdEfHu" role="3clFbG">
              <node concept="1Wqviy" id="6bosOKdEfSu" role="37vLTx" />
              <node concept="2OqwBi" id="6bosOKdEezo" role="37vLTJ">
                <node concept="EsrRn" id="6bosOKdEesd" role="2Oq$k0" />
                <node concept="3TrcHB" id="6bosOKdEeL7" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhdUzN" resolve="breite" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="EnEH3" id="6bosOKdCJQA" role="1MhHOB">
      <ref role="EomxK" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
      <node concept="QB0g5" id="6bosOKdCJQT" role="QCWH9">
        <node concept="3clFbS" id="6bosOKdCJQU" role="2VODD2">
          <node concept="3clFbF" id="6bosOKdCJQV" role="3cqZAp">
            <node concept="1Wc70l" id="6bosOKdCJQW" role="3clFbG">
              <node concept="2dkUwp" id="6bosOKdCJQX" role="3uHU7w">
                <node concept="3cmrfG" id="6bosOKdCJQY" role="3uHU7w">
                  <property role="3cmrfH" value="100" />
                </node>
                <node concept="1Wqviy" id="6bosOKdCJQZ" role="3uHU7B" />
              </node>
              <node concept="3eOSWO" id="6bosOKdCJR0" role="3uHU7B">
                <node concept="1Wqviy" id="6bosOKdCJR1" role="3uHU7B" />
                <node concept="3cmrfG" id="6bosOKdCJR2" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1LLf8_" id="6bosOKdEguL" role="1LXaQT">
        <node concept="3clFbS" id="6bosOKdEguM" role="2VODD2">
          <node concept="3clFbF" id="6bosOKdEgGL" role="3cqZAp">
            <node concept="37vLTI" id="6bosOKdEi6T" role="3clFbG">
              <node concept="1Wqviy" id="6bosOKdEihS" role="37vLTx" />
              <node concept="2OqwBi" id="6bosOKdEgNV" role="37vLTJ">
                <node concept="EsrRn" id="6bosOKdEgGK" role="2Oq$k0" />
                <node concept="3TrcHB" id="6bosOKdEh1X" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="EnEH3" id="6bosOKdCK4T" role="1MhHOB">
      <ref role="EomxK" to="5xc7:3JjgIMhdUzV" resolve="minen" />
      <node concept="QB0g5" id="6bosOKdCK5n" role="QCWH9">
        <node concept="3clFbS" id="6bosOKdCK5o" role="2VODD2">
          <node concept="3clFbF" id="6bosOKdCKcH" role="3cqZAp">
            <node concept="1Wc70l" id="6bosOKdCM3K" role="3clFbG">
              <node concept="3eOVzh" id="6bosOKdCNiw" role="3uHU7w">
                <node concept="17qRlL" id="6bosOKdCPEp" role="3uHU7w">
                  <node concept="2OqwBi" id="6bosOKdCQgo" role="3uHU7w">
                    <node concept="EsrRn" id="6bosOKdCPXz" role="2Oq$k0" />
                    <node concept="3TrcHB" id="6bosOKdCQKZ" role="2OqNvi">
                      <ref role="3TsBF5" to="5xc7:3JjgIMhdUzN" resolve="breite" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="6bosOKdCO0$" role="3uHU7B">
                    <node concept="EsrRn" id="6bosOKdCNzH" role="2Oq$k0" />
                    <node concept="3TrcHB" id="6bosOKdCO_I" role="2OqNvi">
                      <ref role="3TsBF5" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
                    </node>
                  </node>
                </node>
                <node concept="1Wqviy" id="6bosOKdCMhs" role="3uHU7B" />
              </node>
              <node concept="3eOSWO" id="6bosOKdCL7K" role="3uHU7B">
                <node concept="1Wqviy" id="6bosOKdCKcG" role="3uHU7B" />
                <node concept="3cmrfG" id="6bosOKdCL7Q" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1LLf8_" id="6bosOKdEiLS" role="1LXaQT">
        <node concept="3clFbS" id="6bosOKdEiLT" role="2VODD2">
          <node concept="3clFbF" id="6bosOKdEj1T" role="3cqZAp">
            <node concept="37vLTI" id="6bosOKdEka$" role="3clFbG">
              <node concept="1Wqviy" id="6bosOKdEkl$" role="37vLTx" />
              <node concept="2OqwBi" id="6bosOKdEj93" role="37vLTJ">
                <node concept="EsrRn" id="6bosOKdEj1S" role="2Oq$k0" />
                <node concept="3TrcHB" id="6bosOKdEjgm" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhdUzV" resolve="minen" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

