<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0cf7ad86-3917-467b-886e-bc1b495a34b6(Secret.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="12" />
    <use id="0272d3b4-4cc8-481e-9e2f-07793fbfcb41" name="jetbrains.mps.lang.editor.table" version="0" />
    <use id="b1c7d06f-525d-43b5-9b0a-2fc8f7f076ba" name="jetbrains.mps.editor.contextActionsTool.lang.menus" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="squ6" ref="r:b60215f1-3d3e-41cc-8321-723ef8eb59dd(jetbrains.mps.lang.editor.table.runtime)" />
    <import index="cj4x" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor(MPS.Editor/)" />
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" />
    <import index="5xc7" ref="r:cbde3a60-d7cb-4f79-b8b8-0cf8998202e6(Secret.structure)" implicit="true" />
    <import index="foc1" ref="r:88d149e9-4e5b-4014-8da9-03b7f77d0ebb(Secret.behavior)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1402906326895675325" name="jetbrains.mps.lang.editor.structure.CellActionMap_FunctionParm_selectedNode" flags="nn" index="0IXxy" />
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1597643335227097138" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_TransformationMenu_node" flags="ng" index="7Obwk" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="795210086017940429" name="jetbrains.mps.lang.editor.structure.ReadOnlyStyleClassItem" flags="lg" index="xShMh" />
      <concept id="6718020819487620876" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Default" flags="ng" index="A1WHr" />
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1638911550608571617" name="jetbrains.mps.lang.editor.structure.TransformationMenu_Default" flags="ng" index="IW6AY" />
      <concept id="1638911550608610798" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Execute" flags="ig" index="IWg2L" />
      <concept id="1638911550608610278" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Action" flags="ng" index="IWgqT">
        <child id="1638911550608610281" name="executeFunction" index="IWgqQ" />
        <child id="5692353713941573325" name="textFunction" index="1hCUd6" />
      </concept>
      <concept id="1186402211651" name="jetbrains.mps.lang.editor.structure.StyleSheet" flags="ng" index="V5hpn">
        <child id="1186402402630" name="styleClass" index="V601i" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186404574412" name="jetbrains.mps.lang.editor.structure.BackgroundColorStyleClassItem" flags="ln" index="Veino" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
        <child id="1223387335081" name="query" index="3n$kyP" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1186415722038" name="jetbrains.mps.lang.editor.structure.FontSizeStyleClassItem" flags="ln" index="VSNWy">
        <property id="1221209241505" name="value" index="1lJzqX" />
      </concept>
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="3383245079137382180" name="jetbrains.mps.lang.editor.structure.StyleClass" flags="ig" index="14StLt" />
      <concept id="1139535219966" name="jetbrains.mps.lang.editor.structure.CellActionMapDeclaration" flags="ig" index="1h_SRR">
        <reference id="1139535219968" name="applicableConcept" index="1h_SK9" />
        <child id="1139535219969" name="item" index="1h_SK8" />
      </concept>
      <concept id="1139535280617" name="jetbrains.mps.lang.editor.structure.CellActionMapItem" flags="lg" index="1hA7zw">
        <property id="1139535298778" name="actionId" index="1hAc7j" />
        <child id="1139535280620" name="executeFunction" index="1hA7z_" />
      </concept>
      <concept id="1139535439104" name="jetbrains.mps.lang.editor.structure.CellActionMap_ExecuteFunction" flags="in" index="1hAIg9" />
      <concept id="5692353713941573329" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_ActionLabelText" flags="ig" index="1hCUdq" />
      <concept id="7291101478617127464" name="jetbrains.mps.lang.editor.structure.IExtensibleTransformationMenuPart" flags="ng" index="1joUw2">
        <child id="8954657570916349207" name="features" index="2jZA2a" />
      </concept>
      <concept id="1223387125302" name="jetbrains.mps.lang.editor.structure.QueryFunction_Boolean" flags="in" index="3nzxsE" />
      <concept id="9122903797336200704" name="jetbrains.mps.lang.editor.structure.ApplyStyleClassCondition" flags="lg" index="1uO$qF">
        <child id="9122903797336200706" name="query" index="1uO$qD" />
      </concept>
      <concept id="9122903797312246523" name="jetbrains.mps.lang.editor.structure.StyleReference" flags="ng" index="1wgc9g">
        <reference id="9122903797312247166" name="style" index="1wgcnl" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <reference id="1139959269582" name="actionMap" index="1ERwB7" />
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
        <child id="4202667662392416064" name="transformationMenu" index="3vIgyS" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <property id="1160590353935" name="usesFolding" index="S$Qs1" />
        <property id="6240706158490734113" name="collapseByDefault" index="3EXrWe" />
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="7723470090030138869" name="foldedCellModel" index="AHCbl" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="5624877018228267058" name="jetbrains.mps.lang.editor.structure.ITransformationMenu" flags="ng" index="3INCJE">
        <child id="1638911550608572412" name="sections" index="IW6Ez" />
      </concept>
      <concept id="1161622981231" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1Q80Hx" />
      <concept id="7980428675268276156" name="jetbrains.mps.lang.editor.structure.TransformationMenuSection" flags="ng" index="1Qtc8_">
        <child id="7980428675268276157" name="locations" index="1Qtc8$" />
        <child id="7980428675268276159" name="parts" index="1Qtc8A" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1950447826681509042" name="jetbrains.mps.lang.editor.structure.ApplyStyleClass" flags="lg" index="3Xmtl4">
        <child id="1950447826683828796" name="target" index="3XvnJa" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_" />
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="b1c7d06f-525d-43b5-9b0a-2fc8f7f076ba" name="jetbrains.mps.editor.contextActionsTool.lang.menus">
      <concept id="8954657570916343208" name="jetbrains.mps.editor.contextActionsTool.lang.menus.structure.TransformationLocation_ContextActionsTool" flags="ng" index="2jZ$wP" />
      <concept id="8954657570916343205" name="jetbrains.mps.editor.contextActionsTool.lang.menus.structure.TransformationFeature_Tooltip" flags="ng" index="2jZ$wS" />
      <concept id="8954657570916342471" name="jetbrains.mps.editor.contextActionsTool.lang.menus.structure.TransformationFeature_Icon" flags="ng" index="2jZ$Xq" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0272d3b4-4cc8-481e-9e2f-07793fbfcb41" name="jetbrains.mps.lang.editor.table">
      <concept id="4490468428501056077" name="jetbrains.mps.lang.editor.table.structure.QueryFunction_TableModel" flags="in" index="2XI2dN" />
      <concept id="4677325677876400523" name="jetbrains.mps.lang.editor.table.structure.CellModel_Table" flags="ng" index="1CiYdB">
        <child id="4490468428501048483" name="tableModel" index="2XI0mt" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="1h_SRR" id="3JjgIMhd6G0">
    <property role="TrG5h" value="Feld_Actions" />
    <ref role="1h_SK9" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
    <node concept="1hA7zw" id="3JjgIMhd6G1" role="1h_SK8">
      <property role="1hAc7j" value="click_action_id" />
      <node concept="1hAIg9" id="3JjgIMhd6G2" role="1hA7z_">
        <node concept="3clFbS" id="3JjgIMhd6G3" role="2VODD2">
          <node concept="3clFbF" id="3JjgIMhdx0n" role="3cqZAp">
            <node concept="2OqwBi" id="3JjgIMhdx7r" role="3clFbG">
              <node concept="0IXxy" id="3JjgIMhdx0m" role="2Oq$k0" />
              <node concept="2qgKlT" id="3JjgIMhdxOj" role="2OqNvi">
                <ref role="37wK5l" to="foc1:3JjgIMhdxA$" resolve="onClick" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JjgIMhdE1s">
    <ref role="1XX52x" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
    <node concept="1HlG4h" id="3JjgIMhdE1x" role="2wV5jI">
      <ref role="1ERwB7" node="3JjgIMhd6G0" resolve="Feld_Actions" />
      <node concept="VPXOz" id="4Tzsyf2OtP9" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="1HfYo3" id="3JjgIMhdE1z" role="1HlULh">
        <node concept="3TQlhw" id="3JjgIMhdE1_" role="1Hhtcw">
          <node concept="3clFbS" id="3JjgIMhdE1B" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhdEah" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhdEnn" role="3clFbG">
                <node concept="pncrf" id="3JjgIMhdEag" role="2Oq$k0" />
                <node concept="2qgKlT" id="3JjgIMhdEHd" role="2OqNvi">
                  <ref role="37wK5l" to="foc1:3JjgIMhd6Gv" resolve="getDarstellung" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="VSNWy" id="3JjgIMhllEy" role="3F10Kt">
        <property role="1lJzqX" value="24" />
      </node>
      <node concept="VPxyj" id="3JjgIMhmnCR" role="3F10Kt" />
      <node concept="VPM3Z" id="3JjgIMhqBGD" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMhqC1n" role="3n$kyP">
          <node concept="3clFbS" id="3JjgIMhqC1o" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhqC8H" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhqD$N" role="3clFbG">
                <node concept="1PxgMI" id="3JjgIMhqDbo" role="2Oq$k0">
                  <node concept="chp4Y" id="3JjgIMhqDjo" role="3oSUPX">
                    <ref role="cht4Q" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
                  </node>
                  <node concept="2OqwBi" id="3JjgIMhqCla" role="1m5AlR">
                    <node concept="pncrf" id="3JjgIMhqC8G" role="2Oq$k0" />
                    <node concept="1mfA1w" id="3JjgIMhqCOU" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3TrcHB" id="3JjgIMhqGMi" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhpKDW" resolve="laufend" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1uO$qF" id="3JjgIMhlZAw" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMhlZAy" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMhlZA$" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhlZV1" role="3cqZAp">
              <node concept="3fqX7Q" id="3JjgIMhlZUZ" role="3clFbG">
                <node concept="2OqwBi" id="3JjgIMhm07G" role="3fr31v">
                  <node concept="pncrf" id="3JjgIMhlZV9" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JjgIMhm0Nr" role="2OqNvi">
                    <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMhlZUR" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhlXMW" resolve="zugedeckt" />
        </node>
      </node>
      <node concept="1uO$qF" id="3JjgIMhm1KJ" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMhm1KL" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMhm1KN" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhm264" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhm2ix" role="3clFbG">
                <node concept="pncrf" id="3JjgIMhm263" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JjgIMhm3m5" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMho8Hi" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhlXN0" resolve="aufgedeckt" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="3JjgIMhdPBK">
    <property role="TrG5h" value="SpielFeldTableModel" />
    <node concept="312cEg" id="3JjgIMhdQ0$" role="jymVt">
      <property role="TrG5h" value="spiel" />
      <node concept="3Tm6S6" id="3JjgIMhdPZj" role="1B3o_S" />
      <node concept="3Tqbb2" id="3JjgIMhdPZY" role="1tU5fm">
        <ref role="ehGHo" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
      </node>
    </node>
    <node concept="312cEg" id="6ptJiFkyGt" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myEditorContext" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="6ptJiFkyGu" role="1B3o_S" />
      <node concept="3uibUv" id="6ptJiFkyGV" role="1tU5fm">
        <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
      </node>
    </node>
    <node concept="2tJIrI" id="3JjgIMhdPYF" role="jymVt" />
    <node concept="3Tm1VV" id="3JjgIMhdPBL" role="1B3o_S" />
    <node concept="3uibUv" id="3JjgIMhdPW_" role="1zkMxy">
      <ref role="3uigEE" to="squ6:C$5wo1fOXD" resolve="AbstractTableModel" />
    </node>
    <node concept="3clFbW" id="3JjgIMhdPXn" role="jymVt">
      <node concept="3Tm1VV" id="3JjgIMhdPXp" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhdPXq" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhdPXr" role="3clF47">
        <node concept="3clFbF" id="3JjgIMhdSNA" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhdT_a" role="3clFbG">
            <node concept="37vLTw" id="3JjgIMhdTD6" role="37vLTx">
              <ref role="3cqZAo" node="3JjgIMhdSGq" resolve="spiel" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhdT0V" role="37vLTJ">
              <node concept="Xjq3P" id="3JjgIMhdSN_" role="2Oq$k0" />
              <node concept="2OwXpG" id="3JjgIMhdTl8" role="2OqNvi">
                <ref role="2Oxat5" node="3JjgIMhdQ0$" resolve="spiel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhdTGo" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhdUsD" role="3clFbG">
            <node concept="37vLTw" id="3JjgIMhdUw2" role="37vLTx">
              <ref role="3cqZAo" node="3JjgIMhdSJI" resolve="context" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhdTSq" role="37vLTJ">
              <node concept="Xjq3P" id="3JjgIMhdTGm" role="2Oq$k0" />
              <node concept="2OwXpG" id="3JjgIMhdUdp" role="2OqNvi">
                <ref role="2Oxat5" node="6ptJiFkyGt" resolve="myEditorContext" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="3JjgIMhdSGq" role="3clF46">
        <property role="TrG5h" value="spiel" />
        <property role="3TUv4t" value="true" />
        <node concept="3Tqbb2" id="3JjgIMhdSGp" role="1tU5fm">
          <ref role="ehGHo" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
        </node>
      </node>
      <node concept="37vLTG" id="3JjgIMhdSJI" role="3clF46">
        <property role="TrG5h" value="context" />
        <node concept="3uibUv" id="3JjgIMhdSLr" role="1tU5fm">
          <ref role="3uigEE" to="cj4x:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="3JjgIMhelho" role="jymVt">
      <property role="TrG5h" value="createElement" />
      <node concept="37vLTG" id="3JjgIMhelhp" role="3clF46">
        <property role="TrG5h" value="row" />
        <node concept="10Oyi0" id="3JjgIMhelhq" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="3JjgIMhelhr" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhelht" role="3clF45" />
      <node concept="37vLTG" id="3JjgIMhelhu" role="3clF46">
        <property role="TrG5h" value="column" />
        <node concept="10Oyi0" id="3JjgIMhelhv" role="1tU5fm" />
      </node>
      <node concept="2AHcQZ" id="3JjgIMhelhw" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="3JjgIMhelhx" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhelv6" role="3cqZAp" />
      </node>
    </node>
    <node concept="2tJIrI" id="3JjgIMhelvj" role="jymVt" />
    <node concept="3clFb_" id="3JjgIMhel_8" role="jymVt">
      <property role="TrG5h" value="deleteRow" />
      <node concept="3Tm1VV" id="3JjgIMhel_9" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhel_a" role="3clF45" />
      <node concept="37vLTG" id="3JjgIMhel_b" role="3clF46">
        <property role="TrG5h" value="rowNumber" />
        <node concept="10Oyi0" id="3JjgIMhel_c" role="1tU5fm" />
      </node>
      <node concept="2AHcQZ" id="3JjgIMhel_e" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="3JjgIMhel_f" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhelJM" role="3cqZAp" />
      </node>
    </node>
    <node concept="3clFb_" id="3JjgIMhelTD" role="jymVt">
      <property role="TrG5h" value="getColumnCount" />
      <node concept="3Tm1VV" id="3JjgIMhelTE" role="1B3o_S" />
      <node concept="10Oyi0" id="3JjgIMhelTF" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhelTI" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhem4F" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMhemtK" role="3cqZAk">
            <node concept="37vLTw" id="3JjgIMhemd6" role="2Oq$k0">
              <ref role="3cqZAo" node="3JjgIMhdQ0$" resolve="spiel" />
            </node>
            <node concept="3TrcHB" id="3JjgIMhemIr" role="2OqNvi">
              <ref role="3TsBF5" to="5xc7:3JjgIMhdUzN" resolve="breite" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3JjgIMhelTJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="3JjgIMhenjs" role="jymVt">
      <property role="TrG5h" value="getRowCount" />
      <node concept="10Oyi0" id="3JjgIMhenjt" role="3clF45" />
      <node concept="3Tm1VV" id="3JjgIMhenjv" role="1B3o_S" />
      <node concept="3clFbS" id="3JjgIMhenjx" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhenK4" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMheogB" role="3cqZAk">
            <node concept="37vLTw" id="3JjgIMhenW4" role="2Oq$k0">
              <ref role="3cqZAo" node="3JjgIMhdQ0$" resolve="spiel" />
            </node>
            <node concept="3TrcHB" id="3JjgIMher2w" role="2OqNvi">
              <ref role="3TsBF5" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3JjgIMhenjy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="3JjgIMheren" role="jymVt" />
    <node concept="3clFb_" id="3JjgIMhersD" role="jymVt">
      <property role="TrG5h" value="deleteColumn" />
      <node concept="37vLTG" id="3JjgIMhersF" role="3clF46">
        <property role="TrG5h" value="columnNumber" />
        <node concept="10Oyi0" id="3JjgIMhersG" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="3JjgIMhersH" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhersI" role="3clF45" />
      <node concept="2AHcQZ" id="3JjgIMhersJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="3JjgIMhersK" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMherQS" role="3cqZAp" />
      </node>
    </node>
    <node concept="2tJIrI" id="3JjgIMhescN" role="jymVt" />
    <node concept="3clFb_" id="3JjgIMhessT" role="jymVt">
      <property role="TrG5h" value="getValueAt" />
      <node concept="3Tm1VV" id="3JjgIMhessU" role="1B3o_S" />
      <node concept="3Tqbb2" id="3JjgIMhessW" role="3clF45" />
      <node concept="37vLTG" id="3JjgIMhessX" role="3clF46">
        <property role="TrG5h" value="row" />
        <node concept="10Oyi0" id="3JjgIMhessY" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="3JjgIMhessZ" role="3clF46">
        <property role="TrG5h" value="column" />
        <node concept="10Oyi0" id="3JjgIMhest0" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="3JjgIMhest2" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhesLj" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMheZ$I" role="3cqZAk">
            <node concept="2OqwBi" id="3JjgIMhe$nU" role="2Oq$k0">
              <node concept="2OqwBi" id="3JjgIMhetp8" role="2Oq$k0">
                <node concept="37vLTw" id="3JjgIMhet1n" role="2Oq$k0">
                  <ref role="3cqZAo" node="3JjgIMhdQ0$" resolve="spiel" />
                </node>
                <node concept="3Tsc0h" id="3JjgIMhewf3" role="2OqNvi">
                  <ref role="3TtcxE" to="5xc7:3JjgIMhd6Gg" resolve="felder" />
                </node>
              </node>
              <node concept="3zZkjj" id="3JjgIMheDxH" role="2OqNvi">
                <node concept="1bVj0M" id="3JjgIMheDxJ" role="23t8la">
                  <node concept="3clFbS" id="3JjgIMheDxK" role="1bW5cS">
                    <node concept="3clFbF" id="3JjgIMheDR$" role="3cqZAp">
                      <node concept="1Wc70l" id="3JjgIMheOkR" role="3clFbG">
                        <node concept="3clFbC" id="3JjgIMheY2Z" role="3uHU7w">
                          <node concept="37vLTw" id="3JjgIMheYI$" role="3uHU7w">
                            <ref role="3cqZAo" node="3JjgIMhessX" resolve="row" />
                          </node>
                          <node concept="2OqwBi" id="3JjgIMhePjT" role="3uHU7B">
                            <node concept="37vLTw" id="3JjgIMheP3X" role="2Oq$k0">
                              <ref role="3cqZAo" node="3JjgIMheDxL" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3JjgIMheTla" role="2OqNvi">
                              <ref role="3TsBF5" to="5xc7:3JjgIMhdd4s" resolve="y" />
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbC" id="3JjgIMheMjK" role="3uHU7B">
                          <node concept="2OqwBi" id="3JjgIMheE8m" role="3uHU7B">
                            <node concept="37vLTw" id="3JjgIMheDRz" role="2Oq$k0">
                              <ref role="3cqZAo" node="3JjgIMheDxL" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3JjgIMheHMv" role="2OqNvi">
                              <ref role="3TsBF5" to="5xc7:3JjgIMhdd4o" resolve="x" />
                            </node>
                          </node>
                          <node concept="37vLTw" id="3JjgIMheMYX" role="3uHU7w">
                            <ref role="3cqZAo" node="3JjgIMhessZ" resolve="column" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="3JjgIMheDxL" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="3JjgIMheDxM" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1uHKPH" id="3JjgIMhfby0" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3JjgIMhest3" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="3JjgIMhfc90" role="jymVt">
      <property role="TrG5h" value="insertColumn" />
      <node concept="37vLTG" id="3JjgIMhfc91" role="3clF46">
        <property role="TrG5h" value="columnNumber" />
        <node concept="10Oyi0" id="3JjgIMhfc92" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="3JjgIMhfc93" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhfc94" role="3clF45" />
      <node concept="2AHcQZ" id="3JjgIMhfc96" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="3JjgIMhfc97" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhfd5A" role="3cqZAp" />
      </node>
    </node>
    <node concept="3clFb_" id="3JjgIMhfdUf" role="jymVt">
      <property role="TrG5h" value="insertRow" />
      <node concept="37vLTG" id="3JjgIMhfdUh" role="3clF46">
        <property role="TrG5h" value="rowNumber" />
        <node concept="10Oyi0" id="3JjgIMhfdUi" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="3JjgIMhfdUj" role="3clF45" />
      <node concept="3Tm1VV" id="3JjgIMhfdUk" role="1B3o_S" />
      <node concept="2AHcQZ" id="3JjgIMhfdUl" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="3JjgIMhfdUm" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhfeSH" role="3cqZAp" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JjgIMhfsRV">
    <ref role="1XX52x" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
    <node concept="3EZMnI" id="3JjgIMhpAQF" role="2wV5jI">
      <node concept="3F0ifn" id="6bosOKdDsMm" role="3EZMnx" />
      <node concept="2iRkQZ" id="3JjgIMhpAV$" role="2iSdaV" />
      <node concept="1CiYdB" id="3JjgIMhfsT0" role="3EZMnx">
        <node concept="2XI2dN" id="3JjgIMhfsT2" role="2XI0mt">
          <node concept="3clFbS" id="3JjgIMhfsT4" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhfsW7" role="3cqZAp">
              <node concept="2ShNRf" id="3JjgIMhfsW5" role="3clFbG">
                <node concept="1pGfFk" id="3JjgIMhfudk" role="2ShVmc">
                  <ref role="37wK5l" node="3JjgIMhdPXn" resolve="SpielFeldTableModel" />
                  <node concept="pncrf" id="3JjgIMhfuhN" role="37wK5m" />
                  <node concept="1Q80Hx" id="3JjgIMhfuLu" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="xShMh" id="3JjgIMhqepP" role="3F10Kt">
          <property role="VOm3f" value="true" />
          <node concept="3nzxsE" id="3JjgIMhqeBB" role="3n$kyP">
            <node concept="3clFbS" id="3JjgIMhqeBC" role="2VODD2">
              <node concept="3clFbF" id="3JjgIMhqeIX" role="3cqZAp">
                <node concept="2OqwBi" id="3JjgIMhqeVq" role="3clFbG">
                  <node concept="pncrf" id="3JjgIMhqeIW" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JjgIMhqfpW" role="2OqNvi">
                    <ref role="3TsBF5" to="5xc7:3JjgIMhpKDW" resolve="laufend" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="3JjgIMhpQSy" role="3EZMnx">
        <property role="3F0ifm" value="GEWONNEN!!!!" />
        <node concept="VSNWy" id="3JjgIMhqdF_" role="3F10Kt">
          <property role="1lJzqX" value="24" />
        </node>
        <node concept="pkWqt" id="3JjgIMhpR6w" role="pqm2j">
          <node concept="3clFbS" id="3JjgIMhpR6x" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhpRdQ" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhpRqj" role="3clFbG">
                <node concept="pncrf" id="3JjgIMhpRdP" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JjgIMhpRZ_" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhpKE7" resolve="gewonnen" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="3JjgIMhpB0c" role="3EZMnx">
        <property role="3F0ifm" value="VERLOREN!!!!!!" />
        <node concept="pkWqt" id="3JjgIMhpB4S" role="pqm2j">
          <node concept="3clFbS" id="3JjgIMhpB4T" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhpPJW" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhpPWp" role="3clFbG">
                <node concept="pncrf" id="3JjgIMhpPJV" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JjgIMhpQys" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhpKE1" resolve="verloren" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="VSNWy" id="3JjgIMhqdTq" role="3F10Kt">
          <property role="1lJzqX" value="24" />
        </node>
      </node>
      <node concept="3F0ifn" id="6bosOKdE1aN" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1qu" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1rp" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1sl" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1ti" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1ug" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1I0" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1XL" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1YM" role="3EZMnx" />
      <node concept="3F0ifn" id="6bosOKdE1ZO" role="3EZMnx" />
      <node concept="3EZMnI" id="6bosOKdDfJ$" role="3EZMnx">
        <property role="S$Qs1" value="true" />
        <property role="3EXrWe" value="true" />
        <node concept="3F0ifn" id="6bosOKdDsM2" role="3EZMnx">
          <property role="3F0ifm" value="Einstellungen" />
        </node>
        <node concept="VPM3Z" id="6bosOKdDfJA" role="3F10Kt" />
        <node concept="3EZMnI" id="6bosOKdDfZ9" role="3EZMnx">
          <node concept="VPM3Z" id="6bosOKdDfZb" role="3F10Kt" />
          <node concept="3F0ifn" id="6bosOKdDfZd" role="3EZMnx">
            <property role="3F0ifm" value="Höhe" />
          </node>
          <node concept="3F0ifn" id="6bosOKdDfZm" role="3EZMnx">
            <property role="3F0ifm" value=":" />
          </node>
          <node concept="3F0A7n" id="6bosOKdDfZz" role="3EZMnx">
            <ref role="1NtTu8" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
          </node>
          <node concept="2iRfu4" id="6bosOKdDfZe" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="6bosOKdDfZK" role="3EZMnx">
          <node concept="VPM3Z" id="6bosOKdDfZL" role="3F10Kt" />
          <node concept="3F0ifn" id="6bosOKdDfZM" role="3EZMnx">
            <property role="3F0ifm" value="Breite" />
          </node>
          <node concept="3F0ifn" id="6bosOKdDfZN" role="3EZMnx">
            <property role="3F0ifm" value=":" />
          </node>
          <node concept="3F0A7n" id="6bosOKdDfZO" role="3EZMnx">
            <ref role="1NtTu8" to="5xc7:3JjgIMhdUzN" resolve="breite" />
          </node>
          <node concept="2iRfu4" id="6bosOKdDfZP" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="6bosOKdDg03" role="3EZMnx">
          <node concept="VPM3Z" id="6bosOKdDg05" role="3F10Kt" />
          <node concept="3F0ifn" id="6bosOKdDg07" role="3EZMnx">
            <property role="3F0ifm" value="#-Minen" />
          </node>
          <node concept="3F0ifn" id="6bosOKdDg0s" role="3EZMnx">
            <property role="3F0ifm" value=":" />
          </node>
          <node concept="3F0A7n" id="6bosOKdDg0D" role="3EZMnx">
            <ref role="1NtTu8" to="5xc7:3JjgIMhdUzV" resolve="minen" />
          </node>
          <node concept="2iRfu4" id="6bosOKdDg08" role="2iSdaV" />
        </node>
        <node concept="2EHx9g" id="6bosOKdDfZ2" role="2iSdaV" />
        <node concept="3F0ifn" id="6bosOKdDC$w" role="AHCbl">
          <property role="3F0ifm" value="Einstellungen" />
        </node>
      </node>
      <node concept="3F0ifn" id="6bosOKdDOPa" role="3EZMnx" />
      <node concept="A1WHr" id="5c1u$zfAU9b" role="3vIgyS">
        <ref role="2ZyFGn" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
      </node>
    </node>
  </node>
  <node concept="V5hpn" id="3JjgIMhlXMQ">
    <property role="TrG5h" value="feldStyles" />
    <node concept="14StLt" id="3JjgIMhlXMW" role="V601i">
      <property role="TrG5h" value="zugedeckt" />
      <node concept="Veino" id="3JjgIMhlXNk" role="3F10Kt">
        <property role="Vb096" value="gray" />
      </node>
    </node>
    <node concept="14StLt" id="3JjgIMhlXN0" role="V601i">
      <property role="TrG5h" value="aufgedeckt" />
      <node concept="1uO$qF" id="3JjgIMho8QR" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMho8QT" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMho8QV" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMho8YB" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhoaet" role="3clFbG">
                <node concept="1PxgMI" id="3JjgIMho9Pw" role="2Oq$k0">
                  <node concept="chp4Y" id="3JjgIMho9Xd" role="3oSUPX">
                    <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                  </node>
                  <node concept="pncrf" id="3JjgIMho8YA" role="1m5AlR" />
                </node>
                <node concept="3TrcHB" id="3JjgIMhocY$" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMho8Yu" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhlXN8" resolve="mine" />
        </node>
      </node>
      <node concept="1uO$qF" id="3JjgIMhodjs" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMhodju" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMhodjw" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhodO2" role="3cqZAp">
              <node concept="3fqX7Q" id="3JjgIMhohnI" role="3clFbG">
                <node concept="2OqwBi" id="3JjgIMhohnK" role="3fr31v">
                  <node concept="1PxgMI" id="3JjgIMhohnL" role="2Oq$k0">
                    <node concept="chp4Y" id="3JjgIMhohnM" role="3oSUPX">
                      <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="pncrf" id="3JjgIMhohnN" role="1m5AlR" />
                  </node>
                  <node concept="3TrcHB" id="3JjgIMhohnO" role="2OqNvi">
                    <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMhohyo" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhodDd" resolve="zahl" />
        </node>
      </node>
    </node>
    <node concept="14StLt" id="3JjgIMhlXN8" role="V601i">
      <property role="TrG5h" value="mine" />
      <node concept="Veino" id="3JjgIMhlXNf" role="3F10Kt">
        <property role="Vb096" value="red" />
      </node>
    </node>
    <node concept="14StLt" id="3JjgIMhodDd" role="V601i">
      <property role="TrG5h" value="zahl" />
      <node concept="Veino" id="3JjgIMhoiv4" role="3F10Kt">
        <property role="Vb096" value="lightGray" />
      </node>
      <node concept="1uO$qF" id="3JjgIMhoiv9" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMhoivb" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMhoivd" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhoiAK" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMht5RL" role="3clFbG">
                <node concept="2OqwBi" id="3JjgIMht2rz" role="2Oq$k0">
                  <node concept="1PxgMI" id="3JjgIMhoiQc" role="2Oq$k0">
                    <node concept="chp4Y" id="3JjgIMhoiXT" role="3oSUPX">
                      <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="pncrf" id="3JjgIMhoiAJ" role="1m5AlR" />
                  </node>
                  <node concept="2qgKlT" id="3JjgIMht5lB" role="2OqNvi">
                    <ref role="37wK5l" to="foc1:3JjgIMhd6Gv" resolve="getDarstellung" />
                  </node>
                </node>
                <node concept="liA8E" id="3JjgIMht93C" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.matches(java.lang.String)" resolve="matches" />
                  <node concept="Xl_RD" id="3JjgIMht9gb" role="37wK5m">
                    <property role="Xl_RC" value="1" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMhoiAB" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhlXNu" resolve="eins" />
        </node>
      </node>
      <node concept="1uO$qF" id="3JjgIMho$xr" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMho$xt" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMho$xv" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMho_zI" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhtkNj" role="3clFbG">
                <node concept="2OqwBi" id="3JjgIMho_zL" role="2Oq$k0">
                  <node concept="1PxgMI" id="3JjgIMho_zM" role="2Oq$k0">
                    <node concept="chp4Y" id="3JjgIMho_zN" role="3oSUPX">
                      <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="pncrf" id="3JjgIMho_zO" role="1m5AlR" />
                  </node>
                  <node concept="2qgKlT" id="3JjgIMho_zP" role="2OqNvi">
                    <ref role="37wK5l" to="foc1:3JjgIMhd6Gv" resolve="getDarstellung" />
                  </node>
                </node>
                <node concept="liA8E" id="3JjgIMhtnZa" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.matches(java.lang.String)" resolve="matches" />
                  <node concept="Xl_RD" id="3JjgIMhtobU" role="37wK5m">
                    <property role="Xl_RC" value="2" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMho$Qz" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhlXNN" resolve="zwei" />
        </node>
      </node>
      <node concept="1uO$qF" id="3JjgIMhtyYN" role="3F10Kt">
        <node concept="3nzxsE" id="3JjgIMhtyYO" role="1uO$qD">
          <node concept="3clFbS" id="3JjgIMhtyYP" role="2VODD2">
            <node concept="3clFbF" id="3JjgIMhtyYQ" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhtyYR" role="3clFbG">
                <node concept="2OqwBi" id="3JjgIMhtyYS" role="2Oq$k0">
                  <node concept="1PxgMI" id="3JjgIMhtyYT" role="2Oq$k0">
                    <node concept="chp4Y" id="3JjgIMhtyYU" role="3oSUPX">
                      <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="pncrf" id="3JjgIMhtyYV" role="1m5AlR" />
                  </node>
                  <node concept="2qgKlT" id="3JjgIMhtyYW" role="2OqNvi">
                    <ref role="37wK5l" to="foc1:3JjgIMhd6Gv" resolve="getDarstellung" />
                  </node>
                </node>
                <node concept="liA8E" id="3JjgIMhtyYX" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.matches(java.lang.String)" resolve="matches" />
                  <node concept="Xl_RD" id="3JjgIMhtyYY" role="37wK5m">
                    <property role="Xl_RC" value="3" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="3JjgIMhtzMK" role="3XvnJa">
          <ref role="1wgcnl" node="3JjgIMhtzws" resolve="drei" />
        </node>
      </node>
      <node concept="1uO$qF" id="4Tzsyf2NXR1" role="3F10Kt">
        <node concept="3nzxsE" id="4Tzsyf2NXR3" role="1uO$qD">
          <node concept="3clFbS" id="4Tzsyf2NXR5" role="2VODD2">
            <node concept="3clFbF" id="4Tzsyf2NY9U" role="3cqZAp">
              <node concept="2OqwBi" id="4Tzsyf2NY9W" role="3clFbG">
                <node concept="2OqwBi" id="4Tzsyf2NY9X" role="2Oq$k0">
                  <node concept="1PxgMI" id="4Tzsyf2NY9Y" role="2Oq$k0">
                    <node concept="chp4Y" id="4Tzsyf2NY9Z" role="3oSUPX">
                      <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="pncrf" id="4Tzsyf2NYa0" role="1m5AlR" />
                  </node>
                  <node concept="2qgKlT" id="4Tzsyf2NYa1" role="2OqNvi">
                    <ref role="37wK5l" to="foc1:3JjgIMhd6Gv" resolve="getDarstellung" />
                  </node>
                </node>
                <node concept="liA8E" id="4Tzsyf2NYa2" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.matches(java.lang.String)" resolve="matches" />
                  <node concept="Xl_RD" id="4Tzsyf2NYa3" role="37wK5m">
                    <property role="Xl_RC" value="4" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="4Tzsyf2NY6h" role="3XvnJa">
          <ref role="1wgcnl" node="2nogzNC7ll$" resolve="vier" />
        </node>
      </node>
      <node concept="1uO$qF" id="4Tzsyf2NYNC" role="3F10Kt">
        <node concept="3nzxsE" id="4Tzsyf2NYNE" role="1uO$qD">
          <node concept="3clFbS" id="4Tzsyf2NYNG" role="2VODD2">
            <node concept="3clFbF" id="4Tzsyf2NZ4n" role="3cqZAp">
              <node concept="2OqwBi" id="4Tzsyf2NZ4p" role="3clFbG">
                <node concept="2OqwBi" id="4Tzsyf2NZ4q" role="2Oq$k0">
                  <node concept="1PxgMI" id="4Tzsyf2NZ4r" role="2Oq$k0">
                    <node concept="chp4Y" id="4Tzsyf2NZ4s" role="3oSUPX">
                      <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="pncrf" id="4Tzsyf2NZ4t" role="1m5AlR" />
                  </node>
                  <node concept="2qgKlT" id="4Tzsyf2NZ4u" role="2OqNvi">
                    <ref role="37wK5l" to="foc1:3JjgIMhd6Gv" resolve="getDarstellung" />
                  </node>
                </node>
                <node concept="liA8E" id="4Tzsyf2NZ4v" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.matches(java.lang.String)" resolve="matches" />
                  <node concept="Xl_RD" id="4Tzsyf2NZ4w" role="37wK5m">
                    <property role="Xl_RC" value="5" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1wgc9g" id="4Tzsyf2NZ4f" role="3XvnJa">
          <ref role="1wgcnl" node="2nogzNC7m0m" resolve="funf" />
        </node>
      </node>
    </node>
    <node concept="14StLt" id="3JjgIMhlXNu" role="V601i">
      <property role="TrG5h" value="eins" />
      <node concept="VechU" id="3JjgIMhto_6" role="3F10Kt">
        <property role="Vb096" value="blue" />
      </node>
    </node>
    <node concept="14StLt" id="3JjgIMhlXNN" role="V601i">
      <property role="TrG5h" value="zwei" />
      <node concept="VechU" id="3JjgIMhto_8" role="3F10Kt">
        <property role="Vb096" value="DARK_GREEN" />
      </node>
    </node>
    <node concept="14StLt" id="3JjgIMhtzws" role="V601i">
      <property role="TrG5h" value="drei" />
      <node concept="VechU" id="3JjgIMhtzMI" role="3F10Kt">
        <property role="Vb096" value="red" />
      </node>
    </node>
    <node concept="14StLt" id="2nogzNC7ll$" role="V601i">
      <property role="TrG5h" value="vier" />
      <node concept="VechU" id="2nogzNC7lIH" role="3F10Kt">
        <property role="Vb096" value="DARK_MAGENTA" />
      </node>
    </node>
    <node concept="14StLt" id="2nogzNC7m0m" role="V601i">
      <property role="TrG5h" value="funf" />
      <node concept="VechU" id="2nogzNC7mhY" role="3F10Kt">
        <property role="Vb096" value="orange" />
      </node>
    </node>
  </node>
  <node concept="IW6AY" id="5c1u$zfAk8N">
    <ref role="aqKnT" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
    <node concept="1Qtc8_" id="5c1u$zfAk8O" role="IW6Ez">
      <node concept="IWgqT" id="5c1u$zfAk8X" role="1Qtc8A">
        <node concept="2jZ$Xq" id="5c1u$zfAk8Z" role="2jZA2a" />
        <node concept="2jZ$wS" id="5c1u$zfAk90" role="2jZA2a" />
        <node concept="1hCUdq" id="5c1u$zfAk91" role="1hCUd6">
          <node concept="3clFbS" id="5c1u$zfAk93" role="2VODD2">
            <node concept="3clFbF" id="5c1u$zfAki0" role="3cqZAp">
              <node concept="Xl_RD" id="5c1u$zfAkhZ" role="3clFbG">
                <property role="Xl_RC" value="Neues spiel starten" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="5c1u$zfAk95" role="IWgqQ">
          <node concept="3clFbS" id="5c1u$zfAk97" role="2VODD2">
            <node concept="3clFbF" id="5c1u$zfAkXH" role="3cqZAp">
              <node concept="2OqwBi" id="5c1u$zfAl4L" role="3clFbG">
                <node concept="7Obwk" id="5c1u$zfAkXG" role="2Oq$k0" />
                <node concept="2qgKlT" id="5c1u$zfBYcA" role="2OqNvi">
                  <ref role="37wK5l" to="foc1:5c1u$zfBsm0" resolve="neuStarten" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2jZ$wP" id="5c1u$zfAk8S" role="1Qtc8$" />
    </node>
  </node>
</model>

