<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cbde3a60-d7cb-4f79-b8b8-0cf8998202e6(Secret.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="7" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3JjgIMhd6FZ">
    <property role="EcuMT" value="4310862852139543295" />
    <property role="TrG5h" value="Spiel" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3JjgIMhd6Gg" role="1TKVEi">
      <property role="IQ2ns" value="4310862852139543312" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="felder" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="3JjgIMhd6Gf" resolve="Feld" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhdUzN" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139755763" />
      <property role="TrG5h" value="breite" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhdUzQ" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139755766" />
      <property role="TrG5h" value="hoehe" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhdUzV" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139755771" />
      <property role="TrG5h" value="minen" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhpKDW" role="1TKVEl">
      <property role="IQ2nx" value="4310862852142860924" />
      <property role="TrG5h" value="laufend" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhpKE1" role="1TKVEl">
      <property role="IQ2nx" value="4310862852142860929" />
      <property role="TrG5h" value="verloren" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhpKE7" role="1TKVEl">
      <property role="IQ2nx" value="4310862852142860935" />
      <property role="TrG5h" value="gewonnen" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JjgIMhd6Gf">
    <property role="EcuMT" value="4310862852139543311" />
    <property role="TrG5h" value="Feld" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3JjgIMhdd4o" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139569432" />
      <property role="TrG5h" value="x" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhdd4s" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139569436" />
      <property role="TrG5h" value="y" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhd6Gi" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139543314" />
      <property role="TrG5h" value="mine" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="3JjgIMhd9Et" role="1TKVEl">
      <property role="IQ2nx" value="4310862852139555485" />
      <property role="TrG5h" value="aufgedeckt" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
</model>

