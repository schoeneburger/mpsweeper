<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:88d149e9-4e5b-4014-8da9-03b7f77d0ebb(Secret.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="5xc7" ref="r:cbde3a60-d7cb-4f79-b8b8-0cf8998202e6(Secret.structure)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1153422305557" name="jetbrains.mps.baseLanguage.structure.LessThanOrEqualsExpression" flags="nn" index="2dkUwp" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1200397529627" name="jetbrains.mps.baseLanguage.structure.CharConstant" flags="nn" index="1Xhbcc">
        <property id="1200397540847" name="charConstant" index="1XhdNS" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="4693937538533521280" name="jetbrains.mps.lang.smodel.structure.OfConceptOperation" flags="ng" index="v3k3i">
        <child id="4693937538533538124" name="requestedConcept" index="v3oSu" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1145573345940" name="jetbrains.mps.lang.smodel.structure.Node_GetAllSiblingsOperation" flags="nn" index="2TvwIu" />
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1235566831861" name="jetbrains.mps.baseLanguage.collections.structure.AllOperation" flags="nn" index="2HxqBE" />
      <concept id="1227022210526" name="jetbrains.mps.baseLanguage.collections.structure.ClearAllElementsOperation" flags="nn" index="2Kehj3" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="13h7C7" id="3JjgIMhd6Gk">
    <ref role="13h7C2" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
    <node concept="13i0hz" id="3JjgIMhd6Gv" role="13h7CS">
      <property role="TrG5h" value="getDarstellung" />
      <node concept="3Tm1VV" id="3JjgIMhd6Gw" role="1B3o_S" />
      <node concept="17QB3L" id="3JjgIMhd8Uq" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhd6Gy" role="3clF47">
        <node concept="3clFbJ" id="3JjgIMhdcvJ" role="3cqZAp">
          <node concept="3clFbS" id="3JjgIMhdcvL" role="3clFbx">
            <node concept="3cpWs6" id="3JjgIMhdcQ2" role="3cqZAp">
              <node concept="Xl_RD" id="3JjgIMhdcSn" role="3cqZAk">
                <property role="Xl_RC" value=" " />
              </node>
            </node>
          </node>
          <node concept="3fqX7Q" id="3JjgIMhdcNT" role="3clFbw">
            <node concept="2OqwBi" id="3JjgIMhdcNV" role="3fr31v">
              <node concept="13iPFW" id="3JjgIMhdcNW" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhdcNX" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="3JjgIMhd8Ut" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMhd9r1" role="3clFbw">
            <node concept="13iPFW" id="3JjgIMhd8UR" role="2Oq$k0" />
            <node concept="3TrcHB" id="3JjgIMhd9Cs" role="2OqNvi">
              <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
            </node>
          </node>
          <node concept="3clFbS" id="3JjgIMhd8Uv" role="3clFbx">
            <node concept="3cpWs6" id="3JjgIMhd9Ep" role="3cqZAp">
              <node concept="Xl_RD" id="3JjgIMhdcX0" role="3cqZAk">
                <property role="Xl_RC" value="x" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="3JjgIMhifPa" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMhmyiY" role="3cqZAk">
            <node concept="2YIFZM" id="3JjgIMhiJaf" role="2Oq$k0">
              <ref role="37wK5l" to="wyt6:~String.valueOf(int)" resolve="valueOf" />
              <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
              <node concept="2OqwBi" id="3JjgIMhikjL" role="37wK5m">
                <node concept="2OqwBi" id="3JjgIMhihwP" role="2Oq$k0">
                  <node concept="BsUDl" id="3JjgIMhifUj" role="2Oq$k0">
                    <ref role="37wK5l" node="3JjgIMhdd7p" resolve="getNeighbours" />
                  </node>
                  <node concept="3zZkjj" id="3JjgIMhiipk" role="2OqNvi">
                    <node concept="1bVj0M" id="3JjgIMhiipm" role="23t8la">
                      <node concept="3clFbS" id="3JjgIMhiipn" role="1bW5cS">
                        <node concept="3clFbF" id="3JjgIMhiixL" role="3cqZAp">
                          <node concept="2OqwBi" id="3JjgIMhiiKr" role="3clFbG">
                            <node concept="37vLTw" id="3JjgIMhiixK" role="2Oq$k0">
                              <ref role="3cqZAo" node="3JjgIMhiipo" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3JjgIMhijY4" role="2OqNvi">
                              <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="3JjgIMhiipo" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="3JjgIMhiipp" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="34oBXx" id="3JjgIMhiqpK" role="2OqNvi" />
              </node>
            </node>
            <node concept="liA8E" id="3JjgIMhmCev" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.replace(char,char)" resolve="replace" />
              <node concept="1Xhbcc" id="3JjgIMhmDEY" role="37wK5m">
                <property role="1XhdNS" value="0" />
              </node>
              <node concept="1Xhbcc" id="3JjgIMhmEfJ" role="37wK5m">
                <property role="1XhdNS" value=" " />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3JjgIMhdd1K" role="3cqZAp" />
      </node>
    </node>
    <node concept="13i0hz" id="3JjgIMhdd7p" role="13h7CS">
      <property role="TrG5h" value="getNeighbours" />
      <node concept="3Tm1VV" id="3JjgIMhdd7q" role="1B3o_S" />
      <node concept="3clFbS" id="3JjgIMhdd7s" role="3clF47">
        <node concept="3cpWs6" id="3JjgIMhkcOd" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMhl5Zn" role="3cqZAk">
            <node concept="2OqwBi" id="3JjgIMhkYr2" role="2Oq$k0">
              <node concept="2OqwBi" id="3JjgIMhkTKG" role="2Oq$k0">
                <node concept="13iPFW" id="3JjgIMhkTqV" role="2Oq$k0" />
                <node concept="2TvwIu" id="3JjgIMhkV6m" role="2OqNvi" />
              </node>
              <node concept="v3k3i" id="3JjgIMhl0rP" role="2OqNvi">
                <node concept="chp4Y" id="3JjgIMhl1wR" role="v3oSu">
                  <ref role="cht4Q" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                </node>
              </node>
            </node>
            <node concept="3zZkjj" id="3JjgIMhiUSK" role="2OqNvi">
              <node concept="1bVj0M" id="3JjgIMhiUSM" role="23t8la">
                <node concept="3clFbS" id="3JjgIMhiUSN" role="1bW5cS">
                  <node concept="3clFbF" id="3JjgIMhiV0A" role="3cqZAp">
                    <node concept="1Wc70l" id="3JjgIMhjexP" role="3clFbG">
                      <node concept="2dkUwp" id="3JjgIMhjcIQ" role="3uHU7B">
                        <node concept="2YIFZM" id="3JjgIMhj1Kx" role="3uHU7B">
                          <ref role="37wK5l" to="wyt6:~Math.abs(int)" resolve="abs" />
                          <ref role="1Pybhc" to="wyt6:~Math" resolve="Math" />
                          <node concept="3cpWsd" id="3JjgIMhj72R" role="37wK5m">
                            <node concept="2OqwBi" id="3JjgIMhj7US" role="3uHU7w">
                              <node concept="13iPFW" id="3JjgIMhj732" role="2Oq$k0" />
                              <node concept="3TrcHB" id="3JjgIMhj81G" role="2OqNvi">
                                <ref role="3TsBF5" to="5xc7:3JjgIMhdd4o" resolve="x" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="3JjgIMhj2vo" role="3uHU7B">
                              <node concept="37vLTw" id="3JjgIMhj20S" role="2Oq$k0">
                                <ref role="3cqZAo" node="3JjgIMhiUSO" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="3JjgIMhj4kp" role="2OqNvi">
                                <ref role="3TsBF5" to="5xc7:3JjgIMhdd4o" resolve="x" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3cmrfG" id="3JjgIMhjdj3" role="3uHU7w">
                          <property role="3cmrfH" value="1" />
                        </node>
                      </node>
                      <node concept="2dkUwp" id="3JjgIMhnbIi" role="3uHU7w">
                        <node concept="3cmrfG" id="3JjgIMhncx7" role="3uHU7w">
                          <property role="3cmrfH" value="1" />
                        </node>
                        <node concept="2YIFZM" id="3JjgIMhmTog" role="3uHU7B">
                          <ref role="37wK5l" to="wyt6:~Math.abs(int)" resolve="abs" />
                          <ref role="1Pybhc" to="wyt6:~Math" resolve="Math" />
                          <node concept="3cpWsd" id="3JjgIMhn2jy" role="37wK5m">
                            <node concept="2OqwBi" id="3JjgIMhn3IX" role="3uHU7w">
                              <node concept="13iPFW" id="3JjgIMhn3g_" role="2Oq$k0" />
                              <node concept="3TrcHB" id="3JjgIMhn4Jz" role="2OqNvi">
                                <ref role="3TsBF5" to="5xc7:3JjgIMhdd4s" resolve="y" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="3JjgIMhmVnw" role="3uHU7B">
                              <node concept="37vLTw" id="3JjgIMhmUfK" role="2Oq$k0">
                                <ref role="3cqZAo" node="3JjgIMhiUSO" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="3JjgIMhmY$3" role="2OqNvi">
                                <ref role="3TsBF5" to="5xc7:3JjgIMhdd4s" resolve="y" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="3JjgIMhiUSO" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="3JjgIMhiUSP" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="A3Dl8" id="3JjgIMhl2EQ" role="3clF45">
        <node concept="3Tqbb2" id="3JjgIMhl3Rr" role="A3Ik2">
          <ref role="ehGHo" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="3JjgIMhdxA$" role="13h7CS">
      <property role="TrG5h" value="onClick" />
      <node concept="3Tm1VV" id="3JjgIMhdxA_" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhdxFF" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhdxAB" role="3clF47">
        <node concept="3clFbH" id="3JjgIMhrsK1" role="3cqZAp" />
        <node concept="3clFbJ" id="3JjgIMhdxS$" role="3cqZAp">
          <node concept="3fqX7Q" id="3JjgIMhdxSK" role="3clFbw">
            <node concept="2OqwBi" id="3JjgIMhdy1Q" role="3fr31v">
              <node concept="13iPFW" id="3JjgIMhdxT0" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhdym6" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="3JjgIMhdxSA" role="3clFbx">
            <node concept="3clFbF" id="3JjgIMhdyoa" role="3cqZAp">
              <node concept="37vLTI" id="3JjgIMhdz0f" role="3clFbG">
                <node concept="3clFbT" id="3JjgIMhdz2o" role="37vLTx">
                  <property role="3clFbU" value="true" />
                </node>
                <node concept="2OqwBi" id="3JjgIMhdyvg" role="37vLTJ">
                  <node concept="13iPFW" id="3JjgIMhdyo9" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JjgIMhdyAd" role="2OqNvi">
                    <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JjgIMhoJbr" role="3cqZAp">
              <node concept="3clFbS" id="3JjgIMhoJbt" role="3clFbx">
                <node concept="3clFbF" id="3JjgIMhoJ_U" role="3cqZAp">
                  <node concept="2OqwBi" id="3JjgIMhoOf1" role="3clFbG">
                    <node concept="1PxgMI" id="3JjgIMhoKhJ" role="2Oq$k0">
                      <node concept="chp4Y" id="3JjgIMhoKir" role="3oSUPX">
                        <ref role="cht4Q" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
                      </node>
                      <node concept="2OqwBi" id="3JjgIMhoJIE" role="1m5AlR">
                        <node concept="13iPFW" id="3JjgIMhoJ_S" role="2Oq$k0" />
                        <node concept="1mfA1w" id="3JjgIMhoK2Y" role="2OqNvi" />
                      </node>
                    </node>
                    <node concept="2qgKlT" id="3JjgIMhoVgr" role="2OqNvi">
                      <ref role="37wK5l" node="3JjgIMhoSEy" resolve="mineGeklickt" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3JjgIMhoJmw" role="3clFbw">
                <node concept="13iPFW" id="3JjgIMhoJdK" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JjgIMhoJzV" role="2OqNvi">
                  <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JjgIMhoVt1" role="3cqZAp">
              <node concept="3clFbS" id="3JjgIMhoVt3" role="3clFbx">
                <node concept="3clFbF" id="3JjgIMhp0db" role="3cqZAp">
                  <node concept="2OqwBi" id="3JjgIMhp0Lt" role="3clFbG">
                    <node concept="2OqwBi" id="3JjgIMhp0kh" role="2Oq$k0">
                      <node concept="13iPFW" id="3JjgIMhp0d9" role="2Oq$k0" />
                      <node concept="2qgKlT" id="3JjgIMhp0xX" role="2OqNvi">
                        <ref role="37wK5l" node="3JjgIMhdd7p" resolve="getNeighbours" />
                      </node>
                    </node>
                    <node concept="2es0OD" id="3JjgIMhp2rS" role="2OqNvi">
                      <node concept="1bVj0M" id="3JjgIMhp2rU" role="23t8la">
                        <node concept="3clFbS" id="3JjgIMhp2rV" role="1bW5cS">
                          <node concept="3clFbF" id="3JjgIMhp2yE" role="3cqZAp">
                            <node concept="2OqwBi" id="3JjgIMhp2Fz" role="3clFbG">
                              <node concept="37vLTw" id="3JjgIMhp2yD" role="2Oq$k0">
                                <ref role="3cqZAo" node="3JjgIMhp2rW" resolve="it" />
                              </node>
                              <node concept="2qgKlT" id="3JjgIMhp4sP" role="2OqNvi">
                                <ref role="37wK5l" node="3JjgIMhdxA$" resolve="onClick" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="3JjgIMhp2rW" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="3JjgIMhp2rX" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3JjgIMhoWj5" role="3clFbw">
                <node concept="2OqwBi" id="3JjgIMhoVHa" role="2Oq$k0">
                  <node concept="13iPFW" id="3JjgIMhoV$q" role="2Oq$k0" />
                  <node concept="2qgKlT" id="3JjgIMhoW1k" role="2OqNvi">
                    <ref role="37wK5l" node="3JjgIMhdd7p" resolve="getNeighbours" />
                  </node>
                </node>
                <node concept="2HxqBE" id="3JjgIMhoXXy" role="2OqNvi">
                  <node concept="1bVj0M" id="3JjgIMhoXX$" role="23t8la">
                    <node concept="3clFbS" id="3JjgIMhoXX_" role="1bW5cS">
                      <node concept="3clFbF" id="3JjgIMhoY4n" role="3cqZAp">
                        <node concept="3fqX7Q" id="3JjgIMhp07m" role="3clFbG">
                          <node concept="2OqwBi" id="3JjgIMhp07o" role="3fr31v">
                            <node concept="37vLTw" id="3JjgIMhp07p" role="2Oq$k0">
                              <ref role="3cqZAo" node="3JjgIMhoXXA" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3JjgIMhp07q" role="2OqNvi">
                              <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="3JjgIMhoXXA" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="3JjgIMhoXXB" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3JjgIMhqSIC" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhrhvx" role="3clFbG">
                <node concept="1PxgMI" id="3JjgIMhqTvC" role="2Oq$k0">
                  <node concept="chp4Y" id="3JjgIMhqTwk" role="3oSUPX">
                    <ref role="cht4Q" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
                  </node>
                  <node concept="2OqwBi" id="3JjgIMhqSWe" role="1m5AlR">
                    <node concept="13iPFW" id="3JjgIMhqSIz" role="2Oq$k0" />
                    <node concept="1mfA1w" id="3JjgIMhqTgR" role="2OqNvi" />
                  </node>
                </node>
                <node concept="2qgKlT" id="3JjgIMhrhC_" role="2OqNvi">
                  <ref role="37wK5l" node="3JjgIMhqUrS" resolve="pruefeSieg" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="3JjgIMhrCCV" role="3cqZAp" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="3JjgIMhd6Gl" role="13h7CW">
      <node concept="3clFbS" id="3JjgIMhd6Gm" role="2VODD2">
        <node concept="3clFbF" id="3JjgIMhd9G_" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhdcg1" role="3clFbG">
            <node concept="3clFbT" id="3JjgIMhdclM" role="37vLTx" />
            <node concept="2OqwBi" id="3JjgIMhd9NF" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhd9G$" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhda7V" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="3JjgIMhdU$2">
    <ref role="13h7C2" to="5xc7:3JjgIMhd6FZ" resolve="Spiel" />
    <node concept="13i0hz" id="5c1u$zfBsm0" role="13h7CS">
      <property role="TrG5h" value="neuStarten" />
      <node concept="3Tm1VV" id="5c1u$zfBsm1" role="1B3o_S" />
      <node concept="3cqZAl" id="5c1u$zfBtkR" role="3clF45" />
      <node concept="3clFbS" id="5c1u$zfBsm3" role="3clF47">
        <node concept="3clFbF" id="5c1u$zfAeAB" role="3cqZAp">
          <node concept="2OqwBi" id="5c1u$zfAgDR" role="3clFbG">
            <node concept="2OqwBi" id="5c1u$zfAeWW" role="2Oq$k0">
              <node concept="13iPFW" id="5c1u$zfAeA_" role="2Oq$k0" />
              <node concept="3Tsc0h" id="5c1u$zfAfdB" role="2OqNvi">
                <ref role="3TtcxE" to="5xc7:3JjgIMhd6Gg" resolve="felder" />
              </node>
            </node>
            <node concept="2Kehj3" id="5c1u$zfAhWV" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbF" id="5c1u$zfBBUT" role="3cqZAp">
          <node concept="37vLTI" id="5c1u$zfBBUU" role="3clFbG">
            <node concept="3clFbT" id="5c1u$zfBBUV" role="37vLTx">
              <property role="3clFbU" value="true" />
            </node>
            <node concept="2OqwBi" id="5c1u$zfBBUW" role="37vLTJ">
              <node concept="13iPFW" id="5c1u$zfBBUX" role="2Oq$k0" />
              <node concept="3TrcHB" id="5c1u$zfBBUY" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKDW" resolve="laufend" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5c1u$zfBBUZ" role="3cqZAp">
          <node concept="37vLTI" id="5c1u$zfBBV0" role="3clFbG">
            <node concept="3clFbT" id="5c1u$zfBBV1" role="37vLTx" />
            <node concept="2OqwBi" id="5c1u$zfBBV2" role="37vLTJ">
              <node concept="13iPFW" id="5c1u$zfBBV3" role="2Oq$k0" />
              <node concept="3TrcHB" id="5c1u$zfBBV4" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKE1" resolve="verloren" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5c1u$zfBBV5" role="3cqZAp">
          <node concept="37vLTI" id="5c1u$zfBBV6" role="3clFbG">
            <node concept="3clFbT" id="5c1u$zfBBV7" role="37vLTx" />
            <node concept="2OqwBi" id="5c1u$zfBBV8" role="37vLTJ">
              <node concept="13iPFW" id="5c1u$zfBBV9" role="2Oq$k0" />
              <node concept="3TrcHB" id="5c1u$zfBBVa" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKE7" resolve="gewonnen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5c1u$zfBGbm" role="3cqZAp">
          <node concept="BsUDl" id="5c1u$zfBGbk" role="3clFbG">
            <ref role="37wK5l" node="3JjgIMhdZo0" resolve="initFelder" />
          </node>
        </node>
        <node concept="3clFbH" id="5c1u$zfB_QQ" role="3cqZAp" />
      </node>
    </node>
    <node concept="13i0hz" id="3JjgIMhdZo0" role="13h7CS">
      <property role="TrG5h" value="initFelder" />
      <node concept="3Tm1VV" id="3JjgIMhdZo1" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhdZqU" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhdZo3" role="3clF47">
        <node concept="1Dw8fO" id="3JjgIMhdZxg" role="3cqZAp">
          <node concept="3cpWsn" id="3JjgIMhdZxh" role="1Duv9x">
            <property role="TrG5h" value="i" />
            <node concept="10Oyi0" id="3JjgIMhdZxs" role="1tU5fm" />
            <node concept="3cmrfG" id="3JjgIMhe37R" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3clFbS" id="3JjgIMhdZxi" role="2LFqv$">
            <node concept="1Dw8fO" id="3JjgIMhe3fQ" role="3cqZAp">
              <node concept="3uNrnE" id="3JjgIMhe5UO" role="1Dwrff">
                <node concept="37vLTw" id="3JjgIMhe5UQ" role="2$L3a6">
                  <ref role="3cqZAo" node="3JjgIMhe3fR" resolve="j" />
                </node>
              </node>
              <node concept="3cpWsn" id="3JjgIMhe3fR" role="1Duv9x">
                <property role="TrG5h" value="j" />
                <node concept="10Oyi0" id="3JjgIMhe3g2" role="1tU5fm" />
                <node concept="3cmrfG" id="3JjgIMhe4Rx" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
              <node concept="3clFbS" id="3JjgIMhe3fS" role="2LFqv$">
                <node concept="3cpWs8" id="3JjgIMhecVk" role="3cqZAp">
                  <node concept="3cpWsn" id="3JjgIMhecVn" role="3cpWs9">
                    <property role="TrG5h" value="neuesFeld" />
                    <node concept="3Tqbb2" id="3JjgIMhecVj" role="1tU5fm">
                      <ref role="ehGHo" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                    </node>
                    <node concept="2ShNRf" id="3JjgIMhecVP" role="33vP2m">
                      <node concept="3zrR0B" id="3JjgIMhecVN" role="2ShVmc">
                        <node concept="3Tqbb2" id="3JjgIMhecVO" role="3zrR0E">
                          <ref role="ehGHo" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3JjgIMhecWm" role="3cqZAp">
                  <node concept="37vLTI" id="3JjgIMheeNN" role="3clFbG">
                    <node concept="2OqwBi" id="3JjgIMhed5j" role="37vLTJ">
                      <node concept="37vLTw" id="3JjgIMhecWk" role="2Oq$k0">
                        <ref role="3cqZAo" node="3JjgIMhecVn" resolve="neuesFeld" />
                      </node>
                      <node concept="3TrcHB" id="3JjgIMhedwV" role="2OqNvi">
                        <ref role="3TsBF5" to="5xc7:3JjgIMhdd4o" resolve="x" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="3JjgIMhef9D" role="37vLTx">
                      <ref role="3cqZAo" node="3JjgIMhdZxh" resolve="i" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3JjgIMhefkC" role="3cqZAp">
                  <node concept="37vLTI" id="3JjgIMhefkE" role="3clFbG">
                    <node concept="2OqwBi" id="3JjgIMhefkF" role="37vLTJ">
                      <node concept="37vLTw" id="3JjgIMhefkG" role="2Oq$k0">
                        <ref role="3cqZAo" node="3JjgIMhecVn" resolve="neuesFeld" />
                      </node>
                      <node concept="3TrcHB" id="3JjgIMhefXu" role="2OqNvi">
                        <ref role="3TsBF5" to="5xc7:3JjgIMhdd4s" resolve="y" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="3JjgIMhefw7" role="37vLTx">
                      <ref role="3cqZAo" node="3JjgIMhe3fR" resolve="j" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3JjgIMheg09" role="3cqZAp">
                  <node concept="2OqwBi" id="3JjgIMhehSp" role="3clFbG">
                    <node concept="2OqwBi" id="3JjgIMheg7R" role="2Oq$k0">
                      <node concept="13iPFW" id="3JjgIMheg07" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JjgIMhegll" role="2OqNvi">
                        <ref role="3TtcxE" to="5xc7:3JjgIMhd6Gg" resolve="felder" />
                      </node>
                    </node>
                    <node concept="TSZUe" id="3JjgIMhekMv" role="2OqNvi">
                      <node concept="37vLTw" id="3JjgIMhel04" role="25WWJ7">
                        <ref role="3cqZAo" node="3JjgIMhecVn" resolve="neuesFeld" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbH" id="3JjgIMhefkd" role="3cqZAp" />
              </node>
              <node concept="3eOVzh" id="3JjgIMhe3YO" role="1Dwp0S">
                <node concept="2OqwBi" id="3JjgIMhe4nP" role="3uHU7w">
                  <node concept="13iPFW" id="3JjgIMhe3Z7" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JjgIMhe4G6" role="2OqNvi">
                    <ref role="3TsBF5" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
                  </node>
                </node>
                <node concept="37vLTw" id="3JjgIMhe3gn" role="3uHU7B">
                  <ref role="3cqZAo" node="3JjgIMhe3fR" resolve="j" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOVzh" id="3JjgIMhe1hP" role="1Dwp0S">
            <node concept="2OqwBi" id="3JjgIMhe1EU" role="3uHU7w">
              <node concept="13iPFW" id="3JjgIMhe1ia" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhe1Zd" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhdUzN" resolve="breite" />
              </node>
            </node>
            <node concept="37vLTw" id="3JjgIMhe0uc" role="3uHU7B">
              <ref role="3cqZAo" node="3JjgIMhdZxh" resolve="i" />
            </node>
          </node>
          <node concept="3uNrnE" id="3JjgIMhe2RX" role="1Dwrff">
            <node concept="37vLTw" id="3JjgIMhe2RZ" role="2$L3a6">
              <ref role="3cqZAo" node="3JjgIMhdZxh" resolve="i" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhi4Fh" role="3cqZAp">
          <node concept="BsUDl" id="3JjgIMhi4Ff" role="3clFbG">
            <ref role="37wK5l" node="3JjgIMhf_fO" resolve="placeMines" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="3JjgIMhf_fO" role="13h7CS">
      <property role="TrG5h" value="placeMines" />
      <node concept="3Tm1VV" id="3JjgIMhf_fP" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhf_D4" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhf_fR" role="3clF47">
        <node concept="3cpWs8" id="3JjgIMhfC7A" role="3cqZAp">
          <node concept="3cpWsn" id="3JjgIMhfC7D" role="3cpWs9">
            <property role="TrG5h" value="platzierteMinen" />
            <node concept="10Oyi0" id="3JjgIMhfC7$" role="1tU5fm" />
            <node concept="3cmrfG" id="3JjgIMhfC88" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="3JjgIMhfC7c" role="3cqZAp">
          <node concept="3clFbS" id="3JjgIMhfC7d" role="2LFqv$">
            <node concept="3cpWs8" id="3JjgIMhfEyW" role="3cqZAp">
              <node concept="3cpWsn" id="3JjgIMhfEyZ" role="3cpWs9">
                <property role="TrG5h" value="x" />
                <node concept="10Oyi0" id="3JjgIMhfEyV" role="1tU5fm" />
                <node concept="10QFUN" id="3JjgIMhfHEa" role="33vP2m">
                  <node concept="10Oyi0" id="3JjgIMhfHUK" role="10QFUM" />
                  <node concept="1eOMI4" id="3JjgIMhfHpF" role="10QFUP">
                    <node concept="17qRlL" id="3JjgIMhfFK7" role="1eOMHV">
                      <node concept="2OqwBi" id="3JjgIMhfFU7" role="3uHU7w">
                        <node concept="13iPFW" id="3JjgIMhfFKr" role="2Oq$k0" />
                        <node concept="3TrcHB" id="3JjgIMhfG7C" role="2OqNvi">
                          <ref role="3TsBF5" to="5xc7:3JjgIMhdUzN" resolve="breite" />
                        </node>
                      </node>
                      <node concept="2YIFZM" id="3JjgIMhfEDJ" role="3uHU7B">
                        <ref role="37wK5l" to="wyt6:~Math.random()" resolve="random" />
                        <ref role="1Pybhc" to="wyt6:~Math" resolve="Math" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="3JjgIMhfIry" role="3cqZAp">
              <node concept="3cpWsn" id="3JjgIMhfIrz" role="3cpWs9">
                <property role="TrG5h" value="y" />
                <node concept="10Oyi0" id="3JjgIMhfIr$" role="1tU5fm" />
                <node concept="10QFUN" id="3JjgIMhfIr_" role="33vP2m">
                  <node concept="10Oyi0" id="3JjgIMhfIrA" role="10QFUM" />
                  <node concept="1eOMI4" id="3JjgIMhfIrB" role="10QFUP">
                    <node concept="17qRlL" id="3JjgIMhfIrC" role="1eOMHV">
                      <node concept="2OqwBi" id="3JjgIMhfIrD" role="3uHU7w">
                        <node concept="13iPFW" id="3JjgIMhfIrE" role="2Oq$k0" />
                        <node concept="3TrcHB" id="3JjgIMhfJgX" role="2OqNvi">
                          <ref role="3TsBF5" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
                        </node>
                      </node>
                      <node concept="2YIFZM" id="3JjgIMhfIrG" role="3uHU7B">
                        <ref role="37wK5l" to="wyt6:~Math.random()" resolve="random" />
                        <ref role="1Pybhc" to="wyt6:~Math" resolve="Math" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="3JjgIMhfXVE" role="3cqZAp">
              <node concept="3cpWsn" id="3JjgIMhfXVH" role="3cpWs9">
                <property role="TrG5h" value="kandidat" />
                <node concept="3Tqbb2" id="3JjgIMhfXVC" role="1tU5fm">
                  <ref role="ehGHo" to="5xc7:3JjgIMhd6Gf" resolve="Feld" />
                </node>
                <node concept="2OqwBi" id="3JjgIMhfZ9b" role="33vP2m">
                  <node concept="2OqwBi" id="3JjgIMhfLVn" role="2Oq$k0">
                    <node concept="2OqwBi" id="3JjgIMhfKaC" role="2Oq$k0">
                      <node concept="13iPFW" id="3JjgIMhfJMr" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JjgIMhfKok" role="2OqNvi">
                        <ref role="3TtcxE" to="5xc7:3JjgIMhd6Gg" resolve="felder" />
                      </node>
                    </node>
                    <node concept="3zZkjj" id="3JjgIMhfN_$" role="2OqNvi">
                      <node concept="1bVj0M" id="3JjgIMhfN_A" role="23t8la">
                        <node concept="3clFbS" id="3JjgIMhfN_B" role="1bW5cS">
                          <node concept="3clFbF" id="3JjgIMhfNF2" role="3cqZAp">
                            <node concept="1Wc70l" id="3JjgIMhfSkP" role="3clFbG">
                              <node concept="3clFbC" id="3JjgIMhfW_x" role="3uHU7w">
                                <node concept="37vLTw" id="3JjgIMhfWX5" role="3uHU7w">
                                  <ref role="3cqZAo" node="3JjgIMhfIrz" resolve="y" />
                                </node>
                                <node concept="2OqwBi" id="3JjgIMhfTc9" role="3uHU7B">
                                  <node concept="37vLTw" id="3JjgIMhfSGk" role="2Oq$k0">
                                    <ref role="3cqZAo" node="3JjgIMhfN_C" resolve="it" />
                                  </node>
                                  <node concept="3TrcHB" id="3JjgIMhfUtp" role="2OqNvi">
                                    <ref role="3TsBF5" to="5xc7:3JjgIMhdd4s" resolve="y" />
                                  </node>
                                </node>
                              </node>
                              <node concept="3clFbC" id="3JjgIMhfQOC" role="3uHU7B">
                                <node concept="2OqwBi" id="3JjgIMhfNQF" role="3uHU7B">
                                  <node concept="37vLTw" id="3JjgIMhfNF1" role="2Oq$k0">
                                    <ref role="3cqZAo" node="3JjgIMhfN_C" resolve="it" />
                                  </node>
                                  <node concept="3TrcHB" id="3JjgIMhfOVd" role="2OqNvi">
                                    <ref role="3TsBF5" to="5xc7:3JjgIMhdd4o" resolve="x" />
                                  </node>
                                </node>
                                <node concept="37vLTw" id="3JjgIMhfRfl" role="3uHU7w">
                                  <ref role="3cqZAo" node="3JjgIMhfEyZ" resolve="x" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="3JjgIMhfN_C" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="3JjgIMhfN_D" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="1uHKPH" id="3JjgIMhgQtV" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JjgIMhgRuU" role="3cqZAp">
              <node concept="3clFbS" id="3JjgIMhgRuW" role="3clFbx">
                <node concept="3clFbF" id="3JjgIMhgTCp" role="3cqZAp">
                  <node concept="37vLTI" id="3JjgIMhi3gC" role="3clFbG">
                    <node concept="3clFbT" id="3JjgIMhi3iT" role="37vLTx">
                      <property role="3clFbU" value="true" />
                    </node>
                    <node concept="2OqwBi" id="3JjgIMhgTSc" role="37vLTJ">
                      <node concept="37vLTw" id="3JjgIMhgTCn" role="2Oq$k0">
                        <ref role="3cqZAo" node="3JjgIMhfXVH" resolve="kandidat" />
                      </node>
                      <node concept="3TrcHB" id="3JjgIMhhumq" role="2OqNvi">
                        <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3JjgIMhi3l4" role="3cqZAp">
                  <node concept="3uNrnE" id="3JjgIMhi48L" role="3clFbG">
                    <node concept="37vLTw" id="3JjgIMhi48N" role="2$L3a6">
                      <ref role="3cqZAo" node="3JjgIMhfC7D" resolve="platzierteMinen" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3fqX7Q" id="3JjgIMhgTA6" role="3clFbw">
                <node concept="2OqwBi" id="3JjgIMhgTA8" role="3fr31v">
                  <node concept="37vLTw" id="3JjgIMhgTA9" role="2Oq$k0">
                    <ref role="3cqZAo" node="3JjgIMhfXVH" resolve="kandidat" />
                  </node>
                  <node concept="3TrcHB" id="3JjgIMhgTAa" role="2OqNvi">
                    <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOVzh" id="3JjgIMhfDAR" role="2$JKZa">
            <node concept="2OqwBi" id="3JjgIMhfE6R" role="3uHU7w">
              <node concept="13iPFW" id="3JjgIMhfDBc" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhfEra" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhdUzV" resolve="minen" />
              </node>
            </node>
            <node concept="37vLTw" id="3JjgIMhfC8i" role="3uHU7B">
              <ref role="3cqZAo" node="3JjgIMhfC7D" resolve="platzierteMinen" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="3JjgIMhoSEy" role="13h7CS">
      <property role="TrG5h" value="mineGeklickt" />
      <node concept="3Tm1VV" id="3JjgIMhoSEz" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhoTzY" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhoSE_" role="3clF47">
        <node concept="3clFbF" id="3JjgIMhppNY" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhpMhT" role="3clFbG">
            <node concept="3clFbT" id="3JjgIMhpMk2" role="37vLTx">
              <property role="3clFbU" value="true" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhpLEu" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhpLzp" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhpLRR" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKE1" resolve="verloren" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhqgPs" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhqhA3" role="3clFbG">
            <node concept="3clFbT" id="3JjgIMhqhFG" role="37vLTx" />
            <node concept="2OqwBi" id="3JjgIMhqgWT" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhqgPq" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhqhc1" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKDW" resolve="laufend" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="3JjgIMhqUrS" role="13h7CS">
      <property role="TrG5h" value="pruefeSieg" />
      <node concept="3Tm1VV" id="3JjgIMhqUrT" role="1B3o_S" />
      <node concept="3cqZAl" id="3JjgIMhqVlI" role="3clF45" />
      <node concept="3clFbS" id="3JjgIMhqUrV" role="3clF47">
        <node concept="3clFbJ" id="3JjgIMhqVlN" role="3cqZAp">
          <node concept="3fqX7Q" id="3JjgIMhqVlZ" role="3clFbw">
            <node concept="2OqwBi" id="3JjgIMhqVuT" role="3fr31v">
              <node concept="13iPFW" id="3JjgIMhqVm3" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhqVGq" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKE1" resolve="verloren" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="3JjgIMhqVlP" role="3clFbx">
            <node concept="3clFbJ" id="3JjgIMhqVIt" role="3cqZAp">
              <node concept="2OqwBi" id="3JjgIMhqXL_" role="3clFbw">
                <node concept="2OqwBi" id="3JjgIMhqVRp" role="2Oq$k0">
                  <node concept="13iPFW" id="3JjgIMhqVID" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3JjgIMhqW57" role="2OqNvi">
                    <ref role="3TtcxE" to="5xc7:3JjgIMhd6Gg" resolve="felder" />
                  </node>
                </node>
                <node concept="2HxqBE" id="3JjgIMhqZ4u" role="2OqNvi">
                  <node concept="1bVj0M" id="3JjgIMhqZ4w" role="23t8la">
                    <node concept="3clFbS" id="3JjgIMhqZ4x" role="1bW5cS">
                      <node concept="3clFbF" id="3JjgIMhqZ9O" role="3cqZAp">
                        <node concept="22lmx$" id="3JjgIMhreeV" role="3clFbG">
                          <node concept="2OqwBi" id="3JjgIMhrevy" role="3uHU7w">
                            <node concept="37vLTw" id="3JjgIMhrel4" role="2Oq$k0">
                              <ref role="3cqZAo" node="3JjgIMhqZ4y" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3JjgIMhrfvx" role="2OqNvi">
                              <ref role="3TsBF5" to="5xc7:3JjgIMhd6Gi" resolve="mine" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="3JjgIMhr8U7" role="3uHU7B">
                            <node concept="37vLTw" id="3JjgIMhr8Ix" role="2Oq$k0">
                              <ref role="3cqZAo" node="3JjgIMhqZ4y" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="3JjgIMhra5j" role="2OqNvi">
                              <ref role="3TsBF5" to="5xc7:3JjgIMhd9Et" resolve="aufgedeckt" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="3JjgIMhqZ4y" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="3JjgIMhqZ4z" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbS" id="3JjgIMhqVIv" role="3clFbx">
                <node concept="3clFbF" id="3JjgIMhrfD7" role="3cqZAp">
                  <node concept="37vLTI" id="3JjgIMhrgnC" role="3clFbG">
                    <node concept="3clFbT" id="3JjgIMhrgpL" role="37vLTx">
                      <property role="3clFbU" value="true" />
                    </node>
                    <node concept="2OqwBi" id="3JjgIMhrfKd" role="37vLTJ">
                      <node concept="13iPFW" id="3JjgIMhrfD6" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JjgIMhrfXA" role="2OqNvi">
                        <ref role="3TsBF5" to="5xc7:3JjgIMhpKE7" resolve="gewonnen" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3JjgIMhrgxt" role="3cqZAp">
                  <node concept="37vLTI" id="3JjgIMhrhbC" role="3clFbG">
                    <node concept="3clFbT" id="3JjgIMhrhhh" role="37vLTx" />
                    <node concept="2OqwBi" id="3JjgIMhrgCU" role="37vLTJ">
                      <node concept="13iPFW" id="3JjgIMhrgxr" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JjgIMhrgLA" role="2OqNvi">
                        <ref role="3TsBF5" to="5xc7:3JjgIMhpKDW" resolve="laufend" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="3JjgIMhdU$3" role="13h7CW">
      <node concept="3clFbS" id="3JjgIMhdU$4" role="2VODD2">
        <node concept="3clFbF" id="3JjgIMhdUUt" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhdW36" role="3clFbG">
            <node concept="3cmrfG" id="3JjgIMhdW3o" role="37vLTx">
              <property role="3cmrfH" value="10" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhdV1_" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhdUUo" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhdVfu" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhdUzQ" resolve="hoehe" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhdW8l" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhdXJk" role="3clFbG">
            <node concept="3cmrfG" id="3JjgIMhdXLH" role="37vLTx">
              <property role="3cmrfH" value="10" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhdWqm" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhdW8j" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhdWCf" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhdUzN" resolve="breite" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhdY81" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhdZcA" role="3clFbG">
            <node concept="3cmrfG" id="3JjgIMhdZcS" role="37vLTx">
              <property role="3cmrfH" value="10" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhdYg3" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhdY7Z" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhdYoY" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhdUzV" resolve="minen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhflP7" role="3cqZAp">
          <node concept="2OqwBi" id="3JjgIMhflXA" role="3clFbG">
            <node concept="13iPFW" id="3JjgIMhflP5" role="2Oq$k0" />
            <node concept="2qgKlT" id="3JjgIMhfmjI" role="2OqNvi">
              <ref role="37wK5l" node="3JjgIMhdZo0" resolve="initFelder" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhpMux" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhpNwG" role="3clFbG">
            <node concept="3clFbT" id="3JjgIMhpN$M" role="37vLTx">
              <property role="3clFbU" value="true" />
            </node>
            <node concept="2OqwBi" id="3JjgIMhpMAU" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhpMuv" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhpMLB" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKDW" resolve="laufend" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhpNGi" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhpOwn" role="3clFbG">
            <node concept="3clFbT" id="3JjgIMhpO$t" role="37vLTx" />
            <node concept="2OqwBi" id="3JjgIMhpNP2" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhpNGg" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhpO6d" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKE1" resolve="verloren" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3JjgIMhpOGm" role="3cqZAp">
          <node concept="37vLTI" id="3JjgIMhpPqo" role="3clFbG">
            <node concept="3clFbT" id="3JjgIMhpPuu" role="37vLTx" />
            <node concept="2OqwBi" id="3JjgIMhpOPt" role="37vLTJ">
              <node concept="13iPFW" id="3JjgIMhpOGk" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JjgIMhpP0e" role="2OqNvi">
                <ref role="3TsBF5" to="5xc7:3JjgIMhpKE7" resolve="gewonnen" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

