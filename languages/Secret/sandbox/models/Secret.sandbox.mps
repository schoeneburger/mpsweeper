<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cbb63512-7719-465e-b593-6bd8700fc2a8(Secret.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="63cf51ae-9d22-4da5-b5de-5d9f7b5c1c9f" name="Secret" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="63cf51ae-9d22-4da5-b5de-5d9f7b5c1c9f" name="Secret">
      <concept id="4310862852139543295" name="Secret.structure.Spiel" flags="ng" index="2ETMwj">
        <property id="4310862852142860935" name="gewonnen" index="2EH4xF" />
        <property id="4310862852139755771" name="minen" index="2ETeCn" />
        <property id="4310862852139755766" name="hoehe" index="2ETeCq" />
        <property id="4310862852139755763" name="breite" index="2ETeCv" />
        <child id="4310862852139543312" name="felder" index="2ETMBW" />
      </concept>
      <concept id="4310862852139543311" name="Secret.structure.Feld" flags="ng" index="2ETMBz">
        <property id="4310862852139543314" name="mine" index="2ETMBY" />
        <property id="4310862852139569436" name="y" index="2ETTfK" />
        <property id="4310862852139569432" name="x" index="2ETTfO" />
        <property id="4310862852139555485" name="aufgedeckt" index="2ETXxL" />
      </concept>
    </language>
  </registry>
  <node concept="2ETMwj" id="5c1u$zfBotn">
    <property role="2ETeCq" value="10" />
    <property role="2ETeCv" value="10" />
    <property role="2ETeCn" value="10" />
    <property role="2EH4xF" value="true" />
    <node concept="2ETMBz" id="5c1u$zfCdBr" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="0" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBs" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBt" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBu" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBv" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBw" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBx" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBy" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBz" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdB$" role="2ETMBW">
      <property role="2ETTfO" value="0" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdB_" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="0" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBA" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBB" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="2" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBC" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBD" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBE" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBF" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBG" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBH" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBI" role="2ETMBW">
      <property role="2ETTfO" value="1" />
      <property role="2ETTfK" value="9" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBJ" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="0" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBK" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBL" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBM" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBN" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBO" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBP" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBQ" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBR" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBS" role="2ETMBW">
      <property role="2ETTfO" value="2" />
      <property role="2ETTfK" value="9" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBT" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="0" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBU" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBV" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBW" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBX" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBY" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdBZ" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC0" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC1" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="8" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC2" role="2ETMBW">
      <property role="2ETTfO" value="3" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC3" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="0" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC4" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC5" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC6" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC7" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC8" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC9" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCa" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCb" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCc" role="2ETMBW">
      <property role="2ETTfO" value="4" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCd" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="0" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCe" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCf" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCg" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCh" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCi" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCj" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCk" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCl" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCm" role="2ETMBW">
      <property role="2ETTfO" value="5" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCn" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="0" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCo" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCp" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCq" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCr" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCs" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCt" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCu" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCv" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCw" role="2ETMBW">
      <property role="2ETTfO" value="6" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCx" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="0" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCy" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCz" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC$" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdC_" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCA" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCB" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCC" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCD" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCE" role="2ETMBW">
      <property role="2ETTfO" value="7" />
      <property role="2ETTfK" value="9" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCF" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="0" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCG" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCH" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCI" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCJ" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="4" />
      <property role="2ETMBY" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCK" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCL" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCM" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCN" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCO" role="2ETMBW">
      <property role="2ETTfO" value="8" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCP" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="0" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCQ" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="1" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCR" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="2" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCS" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="3" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCT" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="4" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCU" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="5" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCV" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="6" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCW" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="7" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCX" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="8" />
      <property role="2ETXxL" value="true" />
    </node>
    <node concept="2ETMBz" id="5c1u$zfCdCY" role="2ETMBW">
      <property role="2ETTfO" value="9" />
      <property role="2ETTfK" value="9" />
      <property role="2ETXxL" value="true" />
    </node>
  </node>
</model>

