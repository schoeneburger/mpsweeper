<?xml version="1.0" encoding="UTF-8"?>
<solution name="Secret.sandbox" uuid="1899af36-8595-46dc-87c4-7c5b80ee7553" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:63cf51ae-9d22-4da5-b5de-5d9f7b5c1c9f:Secret" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="1899af36-8595-46dc-87c4-7c5b80ee7553(Secret.sandbox)" version="0" />
  </dependencyVersions>
</solution>

